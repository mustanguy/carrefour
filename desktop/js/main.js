$(document).ready(function ()
{
	var nbMag = 4;
	if($('.slider-content.animated').length)
	{
		$('.slider-content.animated').slick(
		{
			dots: false,
			infinite: false,
			speed: 500,
			slidesToShow: 4,
			slidesToScroll: 4,
			variableWidth: true,
			arrows: true,
			prevArrow: '<button type="button" class="slick-prev"><span class="icon-fleche2"></span></button>',
			nextArrow: '<button type="button" class="slick-next"><span class="icon-fleche2"></span></button>',
			responsive: [
				{
					breakpoint: 1200,
					settings:
					{
						slidesToShow: 3,
						slidesToScroll: 3
					}
        },
				{
					breakpoint: 992,
					settings:
					{
						slidesToShow: 2,
						slidesToScroll: 2
					}
        },
				{
					breakpoint: 768,
					settings:
					{
						arrows: false
					}
        },
				{
					breakpoint: 660,
					settings:
					{
						arrows: false,
						slidesToShow: 3,
						slidesToScroll: 3
					}
        },
				{
					breakpoint: 490,
					settings:
					{
						arrows: false,
						slidesToShow: 2,
						slidesToScroll: 2
					}
        },
				{
					breakpoint: 320,
					settings:
					{
						arrows: false,
						slidesToShow: 1,
						slidesToScroll: 1
					}
        }
      ]
		});
	}

	$('.tabbar a').click(function (e)
	{
		e.preventDefault();

		$('.tabbar .tabbar-item.active').removeClass('active');
		$(this).parent().parent().addClass('active');

		var cat = $(this).data('category');

		if(cat == 'all')
		{
			if($('.slider-content.unanimated').length)
			{
				$('.slider-content.unanimated').slick(
				{
					dots: false,
					infinite: false,
					speed: 500,
					slidesToShow: 4,
					slidesToScroll: 4,
					variableWidth: true,
					arrows: true,
					prevArrow: '<button type="button" class="slick-prev"><span class="icon-fleche2"></span></button>',
					nextArrow: '<button type="button" class="slick-next"><span class="icon-fleche2"></span></button>'
				});
				$('.slider-content.unanimated').addClass('animated');
				$('.slider-content.unanimated').removeClass('unanimated');
			}
			$('.sliders .slider').show();
		}
		else
		{
			if($('.sliders .slider[data-category="' + cat + '"]').length)
			{
				$('.sliders .slider').hide();
				$('.sliders .slider[data-category="' + cat + '"] .slider-content.animated').slick('unslick');
				$('.sliders .slider[data-category="' + cat + '"] .slider-content.animated').addClass('unanimated');
				$('.sliders .slider[data-category="' + cat + '"] .slider-content.animated').removeClass('animated');
				$('.sliders .slider[data-category="' + cat + '"]').show();
			}
		}
	});

	$('.slider-item').click(function ()
	{
		if(Modernizr.mq('(max-width: 767px)'))
		{
			var magTitle = $(this).find('.description .mag h6').html();
			if(magTitle === undefined)
			{
				magTitle = $(this).find('.product h6').html();
			}
			var magCat = $(this).find('.description .mag em').html();
			if(magCat === undefined)
			{
				magCat = $(this).find('.product em').html();
			}
			var magPrice = $(this).find('.description .price span').html();
			var magPriceOld = $(this).find('.description .old-price span').html();
			var magText = $(this).find('.description p:first-of-type').html();
			var magTextSub = $(this).find('.description p.small').html();
			var magImg = $(this).find('.cover img').attr('src');

			var mobileCartHtml = '<div class="mobileCartItem-inner"><div class="mobileCartItem-header"><div class="close-mag"><span class="icon-croix-popup"></span></div><h6>' + magTitle + '</h6><em>' + magCat + '</em></div><div class="mobileCartItem-content"><div class="cover"><img src="' + magImg + '" alt="Mag img" /></div><div class="description"><div class="prices"><strong class="price">' + magPrice + '</strong><span class="old-price strike">' + magPriceOld + '</span><p>' + magText + '</p></div><div class="delimiter"></div><div class="more"><span class="small">' + magTextSub + '</span></div></div><div class="action-block"><button type="button" name="select-btn" class="btn btn-secondary btn-small select-mag">Sélectionner</button><div class="mask"><div class="icon"><span class="icon-coche"></span></div><p>Votre article a bien été ajouté au panier.</p></div></div></div></div>';
			$('#mobileCartItem').html('');
			$('#mobileCartItem').append(mobileCartHtml);
		}
		else
		{
			if(!$(this).hasClass('slider-item-main'))
			{
				if($(this).find('.item-click').is(':visible'))
				{
					//alert('ok');
					//$(this).find('.item-click').fadeOut();
				}
				else
				{
					$('.slider-item .item-click').hide();
					$(this).find('.item-click').fadeIn();
				}
			}
		}
	});
	$('.item-click .cover').click(function () {
		$(this).parent().fadeOut();
	});

	$('#showCart').click(function (e)
	{
		e.preventDefault();
		if($(this).parent().next().next().is(':visible'))
		{
			$(this).parent().next().next().fadeOut();
		}
		else
		{
			$(this).parent().next().next().fadeIn();
		}
	});

	$('#mobileCart').click(function ()
	{
		if($(this).next().is(':visible'))
		{
			$(this).next().fadeOut();
		}
		else
		{
			$(this).next().fadeIn();
			$("html, body").animate(
			{
				scrollTop: 0
			}, "slow");
		}
	});

	$('#showAddress').click(function (e)
	{
		e.preventDefault();
		$(this).removeClass('visible-xs');
		$(this).addClass('hidden');
		$(this).next().removeClass('hidden-xs');
	});

	$('#changeAddress').click(function (e)
	{
		e.preventDefault();
		$('.popup-change-address').fadeIn();
		if(Modernizr.mq('(max-width: 767px)'))
		{
			$("html, body").animate(
			{
				scrollTop: 0
			}, "slow");
		}
	});

	$('.popup-close').click(function ()
	{
		$(this).parent().parent().parent().fadeOut();
	});

	$('.remove-mag').click(function ()
	{
		$(this).parent().parent().fadeOut();
	});

	$('.remove-action').click(function ()
	{
		$(this).parent().fadeOut(function ()
		{
			$(this).remove();
		});
		nbMag--;
		var cartText = nbMag + " magazines";
		if(nbMag <= 1)
		{
			cartText = cartText.slice(0, -1);
		}

		$('#showCart').html(cartText);
		$('#mobileNbCart').html(nbMag);
	});

	$(document).on('click', '.select-mag', function ()
	{
		$(this).next('.mask').fadeIn();
		$(this).next('.mask').addClass('actif');
		setTimeout(function () {
			$('.mask.actif').fadeOut();
			$('.mask.actif').removeClass('actif');
		},2000);
		
		nbMag++;
		var cartText = nbMag + " magazines";
		if(nbMag <= 1)
		{
			cartText = cartText.slice(0, -1);
		}
		$('#showCart').html(cartText);
		$('#mobileNbCart').html(nbMag);

		// TODO Add append in cart-list

	});

	$('.close-cart').click(function ()
	{
		$(this).parent().parent().fadeOut();
	});

	$(document).on('click', '#mobileCartItem .close-mag', function ()
	{
		$('#mobileCartItem').html('');
	});

	$('footer.footer a ').click(function (e)
	{
		e.preventDefault();
	});
});
